package data;

import java.util.ArrayList;
import java.util.Arrays;

import model.Bicicleta;
import model.BicicletaForm;
import model.Totem;
import model.Tranca;
import model.TrancaForm;

public class DB {
	DB() {
		// Static
	}

	private static ArrayList<Bicicleta> bicicletas = new ArrayList<Bicicleta>(
			Arrays.asList(new Bicicleta("Marca 1", "modelo 1", "2001", 1, StatusBicicleta.EM_USO.getName()),
					new Bicicleta("Marca 2", "modelo 2", "2002", 2, StatusBicicleta.DISPONIVEL.getName())));
	private static ArrayList<Totem> totens = new ArrayList<Totem>(
			Arrays.asList(new Totem("Botafogo"), new Totem("Carioca")));
	private static ArrayList<Tranca> trancas = new ArrayList<Tranca>(Arrays.asList(
			new Tranca("", 1, "Botafogo", "2001", "modelo 1", StatusTranca.LIVRE.getName()),
			new Tranca("", 5, "", "2020", "modelo novo", StatusTranca.NOVA.getName()),
			new Tranca(bicicletas.get(1).getId(), 2, "Carioca", "2002", "modelo 2", StatusTranca.OCUPADA.getName())));

	public static void addBicicleta(Bicicleta bicicleta) {
		bicicletas.add(bicicleta);
	}

	public static void addTranca(Tranca tranca) {
		trancas.add(tranca);
	}

	public static void integrarBicicleta(String idTranca, String idBicicleta) {
		Bicicleta bicicleta = getBicicleta(idBicicleta);

		if (bicicleta != null) {
			bicicleta.setStatus(StatusBicicleta.DISPONIVEL.getName());
		} else {
			return;
		}

		Tranca tranca = getTranca(idTranca);

		if (tranca != null) {
			tranca.setStatus(StatusTranca.OCUPADA.getName());
			tranca.setBicicleta(idBicicleta);
		}
	}

	public static void integrarTranca(String idTranca, String idTotem) {
		Tranca tranca = getTranca(idTranca);

		if (tranca != null) {
			Totem totem = getTotem(idTotem);

			if (totem != null) {
				tranca.setLocalizacao(totem.getLocalizacao());
				tranca.setStatus(StatusTranca.LIVRE.getName());
			}
		}

	}

	public static void retirarBicicleta(String idTranca, String idBicicleta) {
		Bicicleta bicicleta = getBicicleta(idBicicleta);

		if (bicicleta != null) {
			bicicleta.setStatus(StatusBicicleta.EM_USO.getName());
		} else {
			return;
		}

		Tranca tranca = getTranca(idTranca);

		if (tranca != null) {
			tranca.setStatus(StatusTranca.LIVRE.getName());
			tranca.setBicicleta("");
		}
	}

	public static void retirarTranca(String idTranca) {
		Tranca tranca = getTranca(idTranca);

		if (tranca != null) {
			tranca.setLocalizacao("");
			tranca.setStatus(StatusTranca.EM_REPARO.getName());
		}

	}

	public static void updateStatusBicicleta(String id, String status) {
		for (Bicicleta bicicleta : bicicletas) {
			if (bicicleta.getId().equals(id)) {
				bicicleta.setStatus(status);
				return;
			}
		}
	}

	public static void updateStatusTranca(String id, String status) {
		for (Tranca tranca : trancas) {
			if (tranca.getId().equals(id)) {
				tranca.setStatus(status);
				return;
			}
		}
	}

	public static void updateBicicleta(String id, BicicletaForm bicicletaForm) {
		for (Bicicleta bicicleta : bicicletas) {
			if (bicicleta.getId().equals(id)) {
				bicicleta.setMarca(bicicletaForm.getMarca());
				bicicleta.setModelo(bicicletaForm.getModelo());
				bicicleta.setAno(bicicletaForm.getAno());
				bicicleta.setNumero(bicicletaForm.getNumero());
				bicicleta.setStatus(bicicletaForm.getStatus());
				return;
			}
		}
	}

	public static void updateTotem(String id, String localizacao) {
		for (Totem totem : totens) {
			if (totem.getId().equals(id)) {
				totem.setLocalizacao(localizacao);
				return;
			}
		}
	}

	public static void updateTranca(String id, TrancaForm trancaForm) {
		for (Tranca tranca : trancas) {
			if (tranca.getId().equals(id)) {
				tranca.setNumero(trancaForm.getNumero());
				tranca.setLocalizacao(trancaForm.getLocalizacao());
				tranca.setAnoDeFabricacao(trancaForm.getAnoDeFabricacao());
				tranca.setModelo(trancaForm.getModelo());
				tranca.setStatus(trancaForm.getStatus());
				return;
			}
		}
	}

	public static void addTotem(Totem totem) {
		totens.add(totem);
	}

	public static void deleteBicicleta(String id) {
		for (Bicicleta bicicleta : bicicletas) {
			if (bicicleta.getId().equals(id)) {
				bicicletas.remove(bicicleta);
				return;
			}
		}
	}

	public static void deleteTotem(String id) {
		for (Totem totem : totens) {
			if (totem.getId().equals(id)) {
				totens.remove(totem);
				return;
			}
		}
	}

	public static void deleteTranca(String id) {
		for (Tranca tranca : trancas) {
			if (tranca.getId().equals(id)) {
				trancas.remove(tranca);
				return;
			}
		}
	}

	public static ArrayList<Bicicleta> getBicicletas() {
		return bicicletas;
	}

	public static Bicicleta getBicicleta(String idBicicleta) {
		for (Bicicleta bicicleta : bicicletas) {
			if (bicicleta.getId().equals(idBicicleta)) {
				return bicicleta;
			}
		}
		return null;
	}

	public static Bicicleta getBicicleta(int numero) {
		for (Bicicleta bicicleta : bicicletas) {
			if (bicicleta.getNumero() == numero) {
				return bicicleta;
			}
		}
		return null;
	}

	public static ArrayList<Totem> getTotens() {
		return totens;
	}

	public static ArrayList<Tranca> getTrancas() {
		return trancas;
	}

	public static Tranca getTranca(String idTranca) {
		for (Tranca tranca : trancas) {
			if (tranca.getId().equals(idTranca)) {
				return tranca;
			}
		}
		return null;
	}

	public static Bicicleta getBicicletaFromTranca(String idTranca) {
		for (Tranca tranca : trancas) {
			if (tranca.getId().equals(idTranca)) {
				return getBicicleta(tranca.getBicicleta());
			}
		}
		return null;
	}

	public static Totem getTotem(String idTotem) {
		for (Totem totem : totens) {
			if (totem.getId().equals(idTotem)) {
				return totem;
			}
		}
		return null;
	}

	public static ArrayList<Tranca> getTrancasFromTotem(String idTotem) {
		ArrayList<Tranca> trancasFromTotem = new ArrayList<Tranca>();
		Totem totem = getTotem(idTotem);

		if (totem != null) {
			for (Tranca tranca : trancas) {
				if (tranca.getLocalizacao().equals(totem.getLocalizacao())) {
					trancasFromTotem.add(tranca);
				}
			}
		}

		return trancasFromTotem;
	}

	public static ArrayList<Bicicleta> getBicicletasFromTotem(String idTotem) {
		ArrayList<Bicicleta> bicicletasFromTotem = new ArrayList<Bicicleta>();
		Totem totem = getTotem(idTotem);

		if (totem != null) {
			for (Tranca tranca : trancas) {
				if (tranca.getLocalizacao().equals(totem.getLocalizacao())) {
					Bicicleta bicicleta = getBicicleta(tranca.getBicicleta());

					if (bicicleta != null) {
						bicicletasFromTotem.add(bicicleta);
					} else {
						return bicicletasFromTotem;
					}

				}
			}
		}

		return bicicletasFromTotem;
	}
}
