package data;

public enum StatusTranca {
	LIVRE("LIVRE"), OCUPADA("OCUPADA"), EM_REPARO("EM_REPARO"), APOSENTADA("APOSENTADA"), NOVA("NOVA");

	private String name;

	private StatusTranca(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static boolean isValid(String name) {
		for (StatusTranca statusTranca : StatusTranca.values()) {
			if (statusTranca.getName().equals(name)) {
				return true;
			}
		}

		return false;
	}
}
