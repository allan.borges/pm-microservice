package data;

public enum StatusBicicleta {
	DISPONIVEL("DISPONIVEL"), EM_USO("EM_USO"), REPARO_SOLICITADO("REPARO_SOLICITADO"), EM_REPARO("EM_REPARO"),
	APOSENTADA("APOSENTADA"), NOVA("NOVA");

	private String name;

	private StatusBicicleta(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static boolean isValid(String name) {
		for (StatusBicicleta statusBicicleta : StatusBicicleta.values()) {
			if (statusBicicleta.getName().equals(name)) {
				return true;
			}
		}

		return false;
	}
}
