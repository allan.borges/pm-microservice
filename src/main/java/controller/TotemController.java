package controller;

import data.DB;
import io.javalin.http.Context;
import model.Totem;
import model.TotemForm;
import utils.Response;

public class TotemController {
	private static String pathIdTotem = "idTotem";

	TotemController() {
		// Controller
	}

	public static void getTotens(Context ctx) {
		ctx.json(DB.getTotens().toArray());
		ctx.status(200);
	}

	public static void getTrancas(Context ctx) {
		String idTotem = ctx.pathParam(pathIdTotem);

		try {
			ctx.json(DB.getTotem(idTotem));
		} catch (Exception e) {
			Response.setErrorResponse(idTotem, 422, "Id do totem inválido.", ctx);
			return;
		}

		try {
			ctx.json(DB.getTrancasFromTotem(idTotem));
			ctx.status(200);
		} catch (Exception e) {
			Response.setErrorResponse(idTotem, 404, "Trancas não encontradas.", ctx);
		}
	}

	public static void getBicicletas(Context ctx) {
		String idTotem = ctx.pathParam(pathIdTotem);

		try {
			ctx.json(DB.getTotem(idTotem));
		} catch (Exception e) {
			Response.setErrorResponse(idTotem, 422, "Id do totem inválido.", ctx);
			return;
		}

		try {
			ctx.json(DB.getBicicletasFromTotem(idTotem));
			ctx.status(200);
		} catch (Exception e) {
			Response.setErrorResponse(idTotem, 404, "Bicicletas não encontradas.", ctx);
		}
	}

	public static void createTotem(Context ctx) {
		TotemForm totemForm;

		try {
			totemForm = ctx.bodyAsClass(TotemForm.class);
		} catch (Exception e) {
			Response.setErrorResponse("", 422, "Dados Inválidos: " + e.getMessage(), ctx);
			return;
		}

		String localizacao = totemForm.getLocalizacao();

		if (localizacao != null && !localizacao.equals("")) {
			Totem totem = new Totem(localizacao);

			DB.addTotem(totem);

			try {
				ctx.json(DB.getTotem(totem.getId()));
				ctx.status(200);
				return;
			} catch (Exception e) {
				Response.setErrorResponse("", 422, "Totem não criado: " + e.getMessage(), ctx);
				return;
			}
		} else {
			Response.setErrorResponse("", 422, "Dados Inválidos: existe campo em branco.", ctx);
			return;
		}
	}

	public static void updateTotem(Context ctx) {
		String idTotem = ctx.pathParam(pathIdTotem);

		if (DB.getTotem(idTotem) == null) {
			Response.setErrorResponse(idTotem, 404, "Totem não encontrado.", ctx);
			return;
		}

		TotemForm totemForm;

		try {
			totemForm = ctx.bodyAsClass(TotemForm.class);
		} catch (Exception e) {
			Response.setErrorResponse("", 422, "Dados Inválidos: " + e.getMessage(), ctx);
			return;
		}

		String localizacao = totemForm.getLocalizacao();

		if (localizacao != null && !localizacao.equals("")) {
			DB.updateTotem(idTotem, localizacao);

			try {
				ctx.json(DB.getTotem(idTotem));
				ctx.status(200);
				return;
			} catch (Exception e) {
				Response.setErrorResponse("", 422, "Totem não editado: " + e.getMessage(), ctx);
				return;
			}
		} else {
			Response.setErrorResponse("", 422, "Dados Inválidos: existe campo em branco.", ctx);
			return;
		}
	}

	public static void deleteTotem(Context ctx) {
		String idTotem = ctx.pathParam(pathIdTotem);

		if (DB.getTotem(idTotem) == null) {
			Response.setErrorResponse(idTotem, 404, "Totem não encontrado.", ctx);
			return;
		}

		DB.deleteTotem(idTotem);

		ctx.status(200);
		return;
	}
}
