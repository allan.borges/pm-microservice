package controller;

import java.util.HashMap;
import java.util.Map;

import data.DB;
import data.StatusBicicleta;
import data.StatusTranca;
import io.javalin.http.Context;
import model.Bicicleta;
import model.BicicletaForm;
import model.IntegrarBicicletaForm;
import model.RetirarBicicletaForm;
import model.Tranca;
import utils.Response;

public class BicicletaController {
	private final static String ID_BICICLETA_PATH = "idBicicleta";
	private final static String BICICLETA_NAO_ENCONTRADA = "Bicicleta não encontrada.";
	private final static String DADOS_INVALIDOS = "Dados Inválidos: ";
	private final static String EXISTE_CAMPO_EM_CAMPO = "existe campo em branco.";

	private BicicletaController() {
		// Controller
	}

	public static void getBicicletas(Context ctx) {
		ctx.json(DB.getBicicletas().toArray());
		ctx.status(200);
	}

	public static void getBicicleta(Context ctx) {
		String idBicicleta = ctx.pathParam(ID_BICICLETA_PATH);

		try {
			ctx.json(DB.getBicicleta(idBicicleta));
			ctx.status(200);
		} catch (Exception e) {
			Response.setErrorResponse(idBicicleta, 404, BICICLETA_NAO_ENCONTRADA, ctx);
		}
	}

	public static void createBicicleta(Context ctx) {
		Map<String, Object> bicicletaMap = getBicicletaValues(ctx);

		String marca = bicicletaMap.get("marca").toString();
		String modelo = bicicletaMap.get("modelo").toString();
		String ano = bicicletaMap.get("ano").toString();
		int numero = (int) bicicletaMap.get("numero");
		String status = bicicletaMap.get("status").toString();

		if (marca != null && modelo != null && ano != null && status != null && !modelo.equals("") && !ano.equals("")
				&& numero != 0 && !status.equals("") && !marca.equals("")) {
			Bicicleta bicicleta = new Bicicleta(marca, modelo, ano, numero, status);

			DB.addBicicleta(bicicleta);

			try {
				ctx.json(DB.getBicicleta(numero));
				ctx.status(200);
				return;
			} catch (Exception e) {
				Response.setErrorResponse("", 422, "Bicicleta não criada: " + e.getMessage(), ctx);
				return;
			}
		} else {
			Response.setErrorResponse("", 422, DADOS_INVALIDOS + EXISTE_CAMPO_EM_CAMPO, ctx);
			return;
		}
	}

	public static void integrarBicicletaNaRede(Context ctx) {
		IntegrarBicicletaForm integrarBicicletaForm;

		try {
			integrarBicicletaForm = ctx.bodyAsClass(IntegrarBicicletaForm.class);
		} catch (Exception e) {
			Response.setErrorResponse("", 422, DADOS_INVALIDOS + e.getMessage(), ctx);
			return;
		}

		String idBicicleta = integrarBicicletaForm.getIdBicicleta();
		String idTranca = integrarBicicletaForm.getIdTranca();
		Bicicleta bicicleta = DB.getBicicleta(idBicicleta);
		Tranca tranca = DB.getTranca(idTranca);

		if (idBicicleta != null && idTranca != null && !idBicicleta.equals("") && !idTranca.equals("")) {
			if (bicicleta != null && tranca != null) {
				if (!bicicleta.getStatus().equals(StatusBicicleta.DISPONIVEL.getName())
						&& tranca.getStatus().equals(StatusTranca.LIVRE.getName())) {
					DB.integrarBicicleta(idTranca, idBicicleta);
					ctx.status(200);
				} else {
					Response.setErrorResponse(idBicicleta, 422,
							DADOS_INVALIDOS + "status da bicicleta ou tranca inválido.", ctx);
				}
			} else {
				Response.setErrorResponse("", 422, DADOS_INVALIDOS + "bicicleta ou tranca não encontrado.", ctx);
			}
		} else {
			Response.setErrorResponse("", 422, DADOS_INVALIDOS + EXISTE_CAMPO_EM_CAMPO, ctx);
		}
	}

	public static void retirarBicicletaDaRede(Context ctx) {
		RetirarBicicletaForm retirarBicicletaForm;

		try {
			retirarBicicletaForm = ctx.bodyAsClass(RetirarBicicletaForm.class);
		} catch (Exception e) {
			Response.setErrorResponse("", 422, DADOS_INVALIDOS + e.getMessage(), ctx);
			return;
		}

		String idBicicleta = retirarBicicletaForm.getIdBicicleta();
		String idTranca = retirarBicicletaForm.getIdTranca();
		Bicicleta bicicleta = DB.getBicicleta(idBicicleta);
		Tranca tranca = DB.getTranca(idTranca);

		if (idBicicleta != null && idTranca != null && !idBicicleta.equals("") && !idTranca.equals("")) {
			if (bicicleta != null && tranca != null) {
				if (bicicleta.getStatus().equals(StatusBicicleta.DISPONIVEL.getName())
						&& tranca.getStatus().equals(StatusTranca.OCUPADA.getName())) {
					DB.retirarBicicleta(retirarBicicletaForm.getIdTranca(), retirarBicicletaForm.getIdBicicleta());
					ctx.status(200);
				} else {
					Response.setErrorResponse(idBicicleta, 422,
							DADOS_INVALIDOS + "status da bicicleta ou tranca inválido.", ctx);
				}
				return;
			} else {
				Response.setErrorResponse("", 422, DADOS_INVALIDOS + "bicicleta ou tranca não encontrado.", ctx);
				return;
			}
		} else {
			Response.setErrorResponse("", 422, DADOS_INVALIDOS + EXISTE_CAMPO_EM_CAMPO, ctx);
			return;
		}
	}

	public static void updateStatus(Context ctx) {
		String idBicicleta = ctx.pathParam(ID_BICICLETA_PATH);
		String acao = ctx.pathParam("acao");

		try {
			ctx.json(DB.getBicicleta(idBicicleta));
		} catch (Exception e) {
			Response.setErrorResponse(idBicicleta, 404, BICICLETA_NAO_ENCONTRADA, ctx);
			return;
		}

		if (StatusBicicleta.isValid(acao)) {
			DB.updateStatusBicicleta(idBicicleta, acao);
			ctx.json(DB.getBicicleta(idBicicleta));
			ctx.status(200);
		} else {
			Response.setErrorResponse(idBicicleta, 422, DADOS_INVALIDOS + "status não válido.", ctx);
		}
	}

	public static void updateBicicleta(Context ctx) {
		String idBicicleta = ctx.pathParam(ID_BICICLETA_PATH);

		if (DB.getBicicleta(idBicicleta) == null) {
			Response.setErrorResponse(idBicicleta, 404, BICICLETA_NAO_ENCONTRADA, ctx);
			return;
		}

		BicicletaForm bicicletaForm;

		try {
			bicicletaForm = ctx.bodyAsClass(BicicletaForm.class);
		} catch (Exception e) {
			Response.setErrorResponse("", 422, DADOS_INVALIDOS + e.getMessage(), ctx);
			return;
		}

		String marca = bicicletaForm.getMarca();
		String modelo = bicicletaForm.getModelo();
		String ano = bicicletaForm.getAno();
		int numero = bicicletaForm.getNumero();
		String status = bicicletaForm.getStatus();

		if (marca != null && modelo != null && ano != null && status != null && !marca.equals("") && !modelo.equals("")
				&& !ano.equals("") && numero != 0 && !status.equals("")) {
			DB.updateBicicleta(idBicicleta, bicicletaForm);

			try {
				ctx.json(DB.getBicicleta(idBicicleta));
				ctx.status(200);
				return;
			} catch (Exception e) {
				Response.setErrorResponse("", 422, "Bicicleta não editada: " + e.getMessage(), ctx);
				return;
			}
		} else {
			Response.setErrorResponse("", 422, DADOS_INVALIDOS + EXISTE_CAMPO_EM_CAMPO, ctx);
			return;
		}
	}

	public static void deleteBicicleta(Context ctx) {
		String idBicicleta = ctx.pathParam(ID_BICICLETA_PATH);

		if (DB.getBicicleta(idBicicleta) == null) {
			Response.setErrorResponse(idBicicleta, 404, BICICLETA_NAO_ENCONTRADA, ctx);
			return;
		}

		DB.deleteBicicleta(idBicicleta);

		ctx.status(200);
		return;
	}

	private static Map<String, Object> getBicicletaValues(Context ctx) {
		BicicletaForm bicicletaForm;

		try {
			bicicletaForm = ctx.bodyAsClass(BicicletaForm.class);
		} catch (Exception e) {
			Response.setErrorResponse("", 422, DADOS_INVALIDOS + e.getMessage(), ctx);
			return new HashMap<>();
		}

		String marca = bicicletaForm.getMarca();
		String modelo = bicicletaForm.getModelo();
		String ano = bicicletaForm.getAno();
		int numero = bicicletaForm.getNumero();
		String status = bicicletaForm.getStatus();

		Map<String, Object> data = new HashMap<>();
		data.put("marca", marca);
		data.put("modelo", modelo);
		data.put("ano", ano);
		data.put("numero", numero);
		data.put("status", status);

		return data;
	}
}
