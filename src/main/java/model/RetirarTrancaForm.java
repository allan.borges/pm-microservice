package model;

public class RetirarTrancaForm {
	private String idTotem, idTranca;

	public RetirarTrancaForm() {
		// Form
	}

	public String getIdTotem() {
		return idTotem;
	}

	public String getIdTranca() {
		return idTranca;
	}
}
