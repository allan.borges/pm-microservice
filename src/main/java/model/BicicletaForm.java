package model;

public class BicicletaForm {
	private String marca, modelo, ano;
	private int numero;
	private String status;

	public BicicletaForm() {
		// Form
	}

	public String getMarca() {
		return marca;
	}

	public String getModelo() {
		return modelo;
	}

	public String getAno() {
		return ano;
	}

	public int getNumero() {
		return numero;
	}

	public String getStatus() {
		return status;
	}

}
