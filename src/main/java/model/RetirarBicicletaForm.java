package model;

public class RetirarBicicletaForm {
	private String idTranca, idBicicleta;

	public RetirarBicicletaForm() {
		// Form
	}

	public String getIdTranca() {
		return idTranca;
	}

	public String getIdBicicleta() {
		return idBicicleta;
	}
}
