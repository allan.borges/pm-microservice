package model;

public class TrancaForm {
	private int numero;
	private String localizacao, anoDeFabricacao, modelo, status;

	public TrancaForm() {
		// Form
	}

	public int getNumero() {
		return numero;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public String getAnoDeFabricacao() {
		return anoDeFabricacao;
	}

	public String getModelo() {
		return modelo;
	}

	public String getStatus() {
		return status;
	}

}
