package model;

public class IntegrarBicicletaForm {
	private String idTranca, idBicicleta;

	public IntegrarBicicletaForm() {
		// Form
	}

	public String getIdTranca() {
		return idTranca;
	}

	public String getIdBicicleta() {
		return idBicicleta;
	}
}
