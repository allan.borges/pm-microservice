package model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import utils.JavalinApp;

class TrancaTest {

	private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

	@BeforeAll
	static void init() {
		app.start(7000);
		
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}
	
	@Test
	void getsTranca1Test() {
		Tranca tranca = new Tranca(10, "Botafogo", "2010", "Qualquer modelo", "ativo");
		
		assertEquals("2010", tranca.getAnoDeFabricacao());
		assertEquals("", tranca.getBicicleta());
		assertEquals("Botafogo", tranca.getLocalizacao());
		assertEquals("Qualquer modelo", tranca.getModelo());
		assertEquals(10, tranca.getNumero());
		assertEquals("ativo", tranca.getStatus());
	}
	
	@Test
	void getsTranca2Test() {
		Tranca tranca = new Tranca("asdf", 20, "Urca", "2020", "Outro modelo", "inativo");
		
		assertEquals("2020", tranca.getAnoDeFabricacao());
		assertEquals("asdf", tranca.getBicicleta());
		assertEquals("Urca", tranca.getLocalizacao());
		assertEquals("Outro modelo", tranca.getModelo());
		assertEquals(20, tranca.getNumero());
		assertEquals("inativo", tranca.getStatus());
	}
	
	@Test
	void setsTrancaTest() {
		Tranca tranca = new Tranca(10, "Botafogo", "2010", "Qualquer modelo", "ativo");
		
		tranca.setAnoDeFabricacao("2020");
		tranca.setBicicleta("asdf");
		tranca.setLocalizacao("Urca");
		tranca.setModelo("Outro modelo");
		tranca.setNumero(20);
		tranca.setStatus("inativo");
		
		assertEquals("2020", tranca.getAnoDeFabricacao());
		assertEquals("asdf", tranca.getBicicleta());
		assertEquals("Urca", tranca.getLocalizacao());
		assertEquals("Outro modelo", tranca.getModelo());
		assertEquals(20, tranca.getNumero());
		assertEquals("inativo", tranca.getStatus());
	}

}
