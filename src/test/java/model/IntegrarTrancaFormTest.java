package model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import utils.JavalinApp;

class IntegrarTrancaFormTest {

	private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

	@BeforeAll
	static void init() {
		app.start(7000);
		
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}
	
	@Test
	void getsIntegrarTrancaFormTest() {
		IntegrarTrancaForm integrarTrancaForm = new IntegrarTrancaForm();
		
		assertEquals(null, integrarTrancaForm.getIdTotem());
		assertEquals(null, integrarTrancaForm.getIdTranca());
	}

}
