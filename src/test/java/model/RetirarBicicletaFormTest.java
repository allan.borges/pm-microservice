package model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import utils.JavalinApp;

class RetirarBicicletaFormTest {

	private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

	@BeforeAll
	static void init() {
		app.start(7000);
		
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}
	
	@Test
	void getsRetirarBicicletaFormTest() {
		RetirarBicicletaForm retirarBicicletaForm = new RetirarBicicletaForm();
		
		assertEquals(null, retirarBicicletaForm.getIdBicicleta());
		assertEquals(null, retirarBicicletaForm.getIdTranca());
	}

}
