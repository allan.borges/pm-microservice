package model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import utils.JavalinApp;

class BicicletaTest {

	private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

	@BeforeAll
	static void init() {
		app.start(7000);
		
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}
	
	@Test
	void getsBicicletaTest() {
		Bicicleta bike = new Bicicleta("Qualquer marca", "Qualquer modelo", "2020", 10, "ativo");
		
		assertEquals("Qualquer marca", bike.getMarca());
		assertEquals("Qualquer modelo", bike.getModelo());
		assertEquals("2020", bike.getAno());
		assertEquals(10, bike.getNumero());
		assertEquals("ativo", bike.getStatus());
	}
	
	@Test
	void setsBicicletaTest() {
		Bicicleta bike = new Bicicleta("Qualquer marca", "Qualquer modelo", "2020", 10, "ativo");
		
		bike.setAno("2010");
		bike.setMarca("Outra marca");
		bike.setModelo("Outro modelo");
		bike.setNumero(20);
		bike.setStatus("inativo");
		
		assertEquals("Outra marca", bike.getMarca());
		assertEquals("Outro modelo", bike.getModelo());
		assertEquals("2010", bike.getAno());
		assertEquals(20, bike.getNumero());
		assertEquals("inativo", bike.getStatus());
	}

}
