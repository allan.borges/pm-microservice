package data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import utils.JavalinApp;

class StatusBicicletaTest {

	private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

	@BeforeAll
	static void init() {
		app.start(7000);
		
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}
	
	@Test
	void StatusBicicletaAposentadaTest() {
		StatusBicicleta statusBicicleta = StatusBicicleta.APOSENTADA;
		
		assertEquals("APOSENTADA", statusBicicleta.getName());
	}
	
	@Test
	void StatusBicicletaDisponivelTest() {
		StatusBicicleta statusBicicleta = StatusBicicleta.DISPONIVEL;
		
		assertEquals("DISPONIVEL", statusBicicleta.getName());
	}
	
	@Test
	void StatusBicicletaEmReparoTest() {
		StatusBicicleta statusBicicleta = StatusBicicleta.EM_REPARO;
		
		assertEquals("EM_REPARO", statusBicicleta.getName());
	}
	
	@Test
	void StatusBicicletaEmUsoTest() {
		StatusBicicleta statusBicicleta = StatusBicicleta.EM_USO;
		
		assertEquals("EM_USO", statusBicicleta.getName());
	}
	
	@Test
	void StatusBicicletaNovaTest() {
		StatusBicicleta statusBicicleta = StatusBicicleta.NOVA;
		
		assertEquals("NOVA", statusBicicleta.getName());
	}
	
	@Test
	void StatusBicicletaReparoSolicitadoTest() {
		StatusBicicleta statusBicicleta = StatusBicicleta.REPARO_SOLICITADO;
		
		assertEquals("REPARO_SOLICITADO", statusBicicleta.getName());
	}
	
	@Test
	void isValidTest() {
		assertEquals(true, StatusBicicleta.isValid("REPARO_SOLICITADO"));
	}
	
	@Test
	void isValidFalseTest() {
		assertEquals(false, StatusBicicleta.isValid("QUALQUER"));
	}

}
